#ifndef BONUSDESCR_HPP
#define BONUSDESCR_HPP

#include <QVector3D>
#include <QString>

struct BonusDescr
{
  QString type;
  QVector3D position;
  QVector3D angleVel;

  bool selected;
};

#endif // BONUSDESCR_HPP
