#ifndef DOORDESCR_HPP
#define DOORDESCR_HPP

#include <QVector3D>

struct DoorDescr
{
  QVector3D position;
  float rotation;

  bool selected;
};

#endif // DOORDESCR_HPP
