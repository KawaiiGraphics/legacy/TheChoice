#ifndef CLICKABLELABEL_HPP
#define CLICKABLELABEL_HPP

#include <QLabel>
#include <functional>

class ClickableLabel : public QLabel
{
  Q_OBJECT

public:
  std::function<void()> onClick;

  ClickableLabel(const QString &text);

  // QWidget interface
protected:
  void mouseReleaseEvent(QMouseEvent *event) override final;
};

#endif // CLICKABLELABEL_HPP
