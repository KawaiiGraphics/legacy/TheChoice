#ifndef MUSICMACHINE_HPP
#define MUSICMACHINE_HPP

#include "World.hpp"
#include <unordered_map>

class MusicMachine
{
public:
  enum class State: uint8_t {
    Idle,

    Scratch,
    Space,
    Dystopia,
    Requiem,
    Cyberdance
  };

private:
  World &world;

  State state;

  const static std::unordered_map<State, QString> stateNames;
  Sound *music;

  std::chrono::steady_clock::time_point lastStateChange;

  std::unordered_map<State, uint64_t> stateDurations;

  int minRoomsToBest;
  int minRoomsToWorst;

public:
  MusicMachine(World &world);
  ~MusicMachine();

  void setMinRoomsToBest(int val);
  void setMinRoomsToWorst(int val);

  void start();

  void acquireHP();
  void acquireKarma();

  void lossHP();
  void lossKarma();

  State ending(int roomsTravelled);



  //IMPLEMENT
private:
  void setState(State st);

  void saveCurrentStateDuration();
};

#endif // MUSICMACHINE_HPP
