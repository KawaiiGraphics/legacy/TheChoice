#ifndef KILLERINFO_HPP
#define KILLERINFO_HPP

#include <QString>

struct KillerInfo {
  QString modelName;
  float speed;
};

#endif // KILLERINFO_HPP
