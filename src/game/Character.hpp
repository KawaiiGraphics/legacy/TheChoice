#ifndef CHARACTER_HPP
#define CHARACTER_HPP

#include "World.hpp"
#include "BonusInfo.hpp"
#include "MusicMachine.hpp"
#include <Kawaii3D/KawaiiCamera.hpp>
#include <QKeyEvent>

class Character: public QObject
{
  World &world;
  MusicMachine &music;
  Entity *entity;

  std::unique_ptr<KawaiiSkeletalAnimation::Instance> animation;
  float animationTime;

  KawaiiCamera &cam;

  QVector3D lastEntityPos;

  QVector3D eulerAngles;
  size_t relPointerInputIndex;

  float distance;

  enum InputCommand {
    None = 0,
    Forward = 1,
    Backward = 2,
    Left = 4,
    Right = 8,
    Jump = 16
  };

  uint32_t input;

  std::chrono::time_point<std::chrono::steady_clock> jumpChargeStarted;

public:
  Character(World &world, MusicMachine &music);
  ~Character();

  KawaiiCamera& getCamera();

  void setInputObject(QObject *input);

  Entity& getEntity();

  void collectBonus(const BonusInfo &bonus);

  void releaseMouse();

  // QObject interface
public:
  bool eventFilter(QObject *, QEvent *event) override;



  //IMPLEMENT
private:
  void tick(Entity &characterEntity, float sec_elapsed);
  void updateCamera();

  void rotateView(const QPoint &deltaMouse);

  static InputCommand getInputCommand(QKeyEvent *event);

  float getJumpPower() const;
  void doJump(float jumpPower);
};

#endif // CHARACTER_HPP
