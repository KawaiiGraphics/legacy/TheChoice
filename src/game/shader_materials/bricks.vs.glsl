module MaterialEffectVert;
precision mediump int; precision highp float;

layout(location = ${SIB_TEXCOORD_ATTR_LOCATION}) in highp vec3 texcoord;

layout(location = 2) out highp vec3 texCoord;

Material {
    vec4 scale;
    vec4 brickColor;
    vec4 mortarColor;
    vec2 brickSize;
    vec2 brickPct;

    vec2 brickProps;
    vec2 mortarProps;

    float innerSide;
} material;

void material_effect(in vec3 norm, inout vec4 vert)
{
    vert.xyz *= material.scale.xyz;

    texCoord = vert.xyz;
}

export material_effect;
