#include "Killer.hpp"
#include "Game.hpp"

Killer::Killer(Game &game, const Room::KillerInstance &info):
  entity(game.bringKiller(info.type)),
  path(info.path),
  lastPos(path.cbegin()),
  speed(game.getKillerSpeed(info.type))
{
  entity->setAngleVelocity(info.angleVel);
  entity->setPosition(path.front());
  entity->onTick_main_thread = std::bind(&Killer::onTick, this, std::placeholders::_1, std::placeholders::_2);

  goToNextPos(*entity);
}

Killer::~Killer()
{
  if(entity)
    entity->removeLater();
}

void Killer::goToNextPos(Entity &e)
{
  auto j = lastPos + 1;
  if(j == path.cend())
    j = path.cbegin();

  e.setVelocity((*j - *lastPos).normalized() * speed);
  lastPos = j;
}

void Killer::onTick(Entity &e, [[maybe_unused]] float sec_elapsed)
{
  if(path.size() == 1) return;

  const float distance = (e.getPosition() - *lastPos).lengthSquared();
  if(distance <= 0.05 * 0.05)
    goToNextPos(e);
}

bool Killer::check(Entity *candidate)
{
  if(candidate == entity)
    {
      entity->removeLater();
      entity = nullptr;
      return true;
    }
  return false;
}
