#include "Game.hpp"
#include <sib_utils/ioReadAll.hpp>
#include <sib_utils/Memento/BlobSaver.hpp>
#include <sib_utils/Memento/BlobLoader.hpp>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>

void Game::loadConfig(const QString &fName)
{
  configDir = QDir(fName);
  configDir.cdUp();
  configDir.makeAbsolute();

  QJsonParseError parseError;
  auto jsonObject = QJsonDocument::fromJson(sib_utils::ioReadAll(QFile(fName)), &parseError).object();

  if(parseError.error != QJsonParseError::NoError)
    throw std::runtime_error("Invalid JSON: \"" + parseError.errorString().toStdString() + "\"");

  bool respackLoaded = false;
  if(auto respack_json = jsonObject.value("resource_pack_path"); respack_json.isString())
    {
      std::unique_ptr<sib_utils::memento::Memento> rootMemento;
      rootMemento.reset(sib_utils::memento::BlobLoader().load(QFile(configDir.absoluteFilePath(respack_json.toString()))));
      KawaiiDataUnit::getMementoFactory().loadChildren(*rootMemento, &world.res());
      world.res().waitTasks();
      respackLoaded = true;
    }

  QString player_model;
  if(auto val = jsonObject.value("player_model"); val.isString())
    {
      player_model = val.toString();
      playerModel = world.res().loadModel(configDir.absoluteFilePath(player_model), "Player").first;
    } else
    if(!respackLoaded)
      throw std::runtime_error("Invalid JSON: must contain \'player_model\' string");


  if(auto models = jsonObject.value("models"); models.isArray())
    loadModels(models.toArray());
  else
    if(!respackLoaded)
      throw std::runtime_error("Invalid JSON: must contain \'models\' array");

  if(auto music_json = jsonObject.value("music"); music_json.isObject())
    loadMusic(music_json.toObject());
  else
    if(!respackLoaded)
      throw std::runtime_error("Invalid JSON: must contain \'music\' object");

  if(auto endings_json = jsonObject.value("ending_images"); endings_json.isObject())
    loadEndings(endings_json.toObject());
  else
    throw std::runtime_error("Invalid JSON: must contain \'ending_images\' object");

  if(auto killer_types = jsonObject.value("killer_types"); killer_types.isArray())
    loadKillerTypes(killer_types.toArray());
  else
    throw std::runtime_error("Invalid JSON: must contain \'killer_types\' array");

  if(auto bonus_types = jsonObject.value("bonus_types"); bonus_types.isArray())
    loadBonusTypes(bonus_types.toArray());
  else
    throw std::runtime_error("Invalid JSON: must contain \'bonus_types\' array");

  if(auto rooms_json = jsonObject.value("rooms"); rooms_json.isArray())
    loadRooms(rooms_json.toArray());
  else
    throw std::runtime_error("Invalid JSON: must contain \'rooms\' array");

  if(auto story_json = jsonObject.value("story"); story_json.isArray())
    loadStory(story_json.toArray());
  else
    throw std::runtime_error("Invalid JSON: must contain \'story\' array");

  if(auto dump_json = jsonObject.value("dump_resource_pack"); dump_json.isString())
    {
      world.res().waitTasks();

      auto rootMemento = KawaiiDataUnit::getMementoFactory().saveObjectTreeBinary(world.res());
      sib_utils::memento::BlobSaver().save(QFile(configDir.absoluteFilePath(dump_json.toString())), *rootMemento);
      delete rootMemento;
    }
}

void Game::loadModels(const QJsonArray &json)
{
  for(const auto &i: json)
    {
      const auto modelObj = i.toObject();
      if(!modelObj.value("file").isString())
        throw std::runtime_error("Invalid JSON: each elem of \'models\' must contain \'file\' string");

      if(!modelObj.value("name").isString())
        throw std::runtime_error("Invalid JSON: each elem of \'models\' must contain \'name\' string");

      auto loadingModel = world.res().loadModel(configDir.absoluteFilePath(modelObj.value("file").toString()), modelObj.value("name").toString());

      if(modelObj.value("scale").isArray())
        {
          loadingModel.first.waitForFinished();
          loadingModel.second->scale(Room::vec3FromJson(modelObj.value("scale").toArray()));
        }
    }
}

void Game::loadMusic(const QJsonObject &json)
{
  if(auto val = json.value("scratch_file"); val.isString())
    world.res().createChild<Sound>(configDir.absoluteFilePath(val.toString()))->setObjectName("Scratch");
  else
    throw std::runtime_error("Invalid JSON: \'music\' object must contain \'scratch_file\' string");

  if(auto val = json.value("space_file"); val.isString())
    world.res().createChild<Sound>(configDir.absoluteFilePath(val.toString()))->setObjectName("Space");
  else
    throw std::runtime_error("Invalid JSON: \'music\' object must contain \'space_file\' string");

  if(auto val = json.value("dystopia_file"); val.isString())
    world.res().createChild<Sound>(configDir.absoluteFilePath(val.toString()))->setObjectName("Dystopia");
  else
    throw std::runtime_error("Invalid JSON: \'music\' object must contain \'dystopia_file\' string");

  if(auto val = json.value("requiem_file"); val.isString())
    world.res().createChild<Sound>(configDir.absoluteFilePath(val.toString()))->setObjectName("Requiem");
  else
    throw std::runtime_error("Invalid JSON: \'music\' object must contain \'requiem_file\' string");

  if(auto val = json.value("cyberdance_file"); val.isString())
    world.res().createChild<Sound>(configDir.absoluteFilePath(val.toString()))->setObjectName("Cyberdance");
  else
    throw std::runtime_error("Invalid JSON: \'music\' object must contain \'cyberdance_file\' string");
}

void Game::loadEndings(const QJsonObject &json)
{
  if(auto val = json.value("scratch_file"); val.isString())
    endings[MusicMachine::State::Scratch] = std::make_unique<QImage>(configDir.absoluteFilePath(val.toString()));
  else
    throw std::runtime_error("Invalid JSON: \'ending_images\' object must contain \'scratch_file\' string");

  if(auto val = json.value("space_file"); val.isString())
    endings[MusicMachine::State::Space] = std::make_unique<QImage>(configDir.absoluteFilePath(val.toString()));
  else
    throw std::runtime_error("Invalid JSON: \'ending_images\' object must contain \'space_file\' string");

  if(auto val = json.value("dystopia_file"); val.isString())
    endings[MusicMachine::State::Dystopia] = std::make_unique<QImage>(configDir.absoluteFilePath(val.toString()));
  else
    throw std::runtime_error("Invalid JSON: \'ending_images\' object must contain \'dystopia_file\' string");

  if(auto val = json.value("requiem_file"); val.isString())
    endings[MusicMachine::State::Requiem] = std::make_unique<QImage>(configDir.absoluteFilePath(val.toString()));
  else
    throw std::runtime_error("Invalid JSON: \'ending_images\' object must contain \'requiem_file\' string");

  if(auto val = json.value("cyberdance_file"); val.isString())
    endings[MusicMachine::State::Cyberdance] = std::make_unique<QImage>(configDir.absoluteFilePath(val.toString()));
  else
    throw std::runtime_error("Invalid JSON: \'ending_images\' object must contain \'cyberdance_file\' string");

  if(auto val = json.value("cyberdance_min_rooms"); val.isDouble())
    music.setMinRoomsToBest(val.toInt());

  if(auto val = json.value("requiem_min_rooms"); val.isDouble())
    music.setMinRoomsToWorst(val.toInt());
}

void Game::loadKillerTypes(const QJsonArray &json)
{
  for(const auto &i: json)
    {
      const auto killerObj = i.toObject();

      auto &killer = killerTypes[killerObj.value("type").toString()];
      killer.modelName = killerObj.value("model_name").toString();
      killer.speed = killerObj.value("speed").toDouble();
    }
}

namespace {
  const QHash<QString, BonusInfo::Effect> bonusEffects =
  {
    { QStringLiteral("hp+"), BonusInfo::Effect::IncreaseHp },
    { QStringLiteral("karma+"), BonusInfo::Effect::IncreaseKarma },
    { QStringLiteral("hp-"), BonusInfo::Effect::DecreaseHp },
    { QStringLiteral("karma-"), BonusInfo::Effect::DecreaseKarma }
  };
}

void Game::loadBonusTypes(const QJsonArray &json)
{
  for(const auto &i: json)
    {
      const auto bonusJson = i.toObject();
      auto &bonus = bonusTypes[bonusJson.value("type").toString()];
      bonus.modelName = bonusJson.value("model_name").toString();
      bonus.effect = bonusEffects.value(bonusJson.value("effect").toString().toLower(), BonusInfo::Effect::None);
    }
}

void Game::loadRooms(const QJsonArray &json)
{
  for(const auto &i: json)
    {
      if(!i.isObject() && !(i.isString() && QFile::exists(configDir.absoluteFilePath(i.toString()))) )
        throw std::runtime_error("Invalid JSON: each elem of \'rooms\' must be an object or a file name");

      if(i.isString())
        {
          QJsonParseError err;
          QJsonDocument doc = QJsonDocument::fromJson(sib_utils::ioReadAll(QFile(configDir.absoluteFilePath(i.toString()))), &err);
          if(err.error != QJsonParseError::NoError)
            throw std::runtime_error("Invalid JSON (" + i.toString().toStdString() + "): \"" + err.errorString().toStdString() + "\"");

          rooms.emplace_back(doc.object());
        } else
        rooms.emplace_back(i.toObject());
    }
}

void Game::loadStory(const QJsonArray &json)
{
  for(const auto &i: json)
    {
      if(!i.isDouble())
        throw std::runtime_error("Invalid JSON: each elem of \'story\' must be a number");

      story.push_back(i.toInt());
    }
}
