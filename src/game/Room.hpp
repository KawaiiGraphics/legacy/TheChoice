#ifndef ROOM_HPP
#define ROOM_HPP

#include "KillerInfo.hpp"
#include "BonusInfo.hpp"

#include <QVector2D>
#include <QVector3D>
#include <functional>
#include <QString>
#include <KawaiiWorlds/Location.hpp>

class Room
{
public:
  struct BonusInstance {
    QString type;
    QVector3D position;
    QVector3D angleVel;
    EntityInfo *entityInfo;
  };

  struct KillerInstance {
    QString type;
    std::vector<QVector3D> path;
    QVector3D angleVel;
    EntityInfo *entityInfo;
  };

  struct DoorInstance {
    QVector2D position;
    EntityInfo *entityInfo;
    float rotation;
  };

  struct Decoration {
    QVector3D pos0;
    QVector3D vel0;
    QVector3D angleVel;
    QString model;
    EntityInfo *entityInfo;
    float mass;
  };

private:
  QVector3D size;

  std::vector<BonusInstance> bonuses;
  std::vector<KillerInstance> killers;

  DoorInstance enter;
  std::vector<DoorInstance> escape;

  std::vector<Decoration> staticDecorations;
  std::vector<Decoration> dynamicDecorations;

  Location location;

public:
  Room(const QJsonObject &jsonObject);
  Room(const Room &orig);

  const QVector3D& getSize() const;

  void forallBonuses(const std::function<void(const BonusInstance&)> &func) const;

  void forallKillers(const std::function<void(const KillerInstance&)> &func) const;

  void forallStaticDecorations(const std::function<void(const Decoration&)> &func) const;
  void forallDynamicDecorations(const std::function<void(const Decoration&)> &func) const;

  const DoorInstance &getEnter() const;

  void forallEscapes(const std::function<void (const DoorInstance&)> &func) const;

  static QVector3D vec3FromJson(const QJsonArray &arr);

  void finalizeBonuses(const QHash<QString, BonusInfo> &bonusessType);
  void finalizeKillers(const QHash<QString, KillerInfo> &killersType);

  Location &getLocation();
};

#endif // ROOM_HPP
