#include "Character.hpp"
#include <KawaiiWorlds/input/RelativePointerInput.hpp>
#include <Kawaii3D/Illumination/KawaiiPBR.hpp>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QMatrix4x4>
#include <QWindow>

Character::Character(World &world, MusicMachine &music):
  world(world),
  music(music),
  entity(world.bringEntity("Textured", "Player", ObjectType::Solid)),
  animation(world.res().getAnimation("Player", 0)),
  animationTime(0),
  cam(*world.res().getScene()->createChild<KawaiiCamera>()),
  distance(5),
  input(0)
{
  auto relativePointer = std::make_unique<RelativePointerInput>();
  relativePointer->func = std::bind(&Character::rotateView, this, std::placeholders::_1);
  relPointerInputIndex = world.addInput(std::move(relativePointer));

  entity->onTick = std::bind(&Character::tick, this, std::placeholders::_1, std::placeholders::_2);
  updateCamera();
}

Character::~Character()
{
}

KawaiiCamera &Character::getCamera()
{
  return cam;
}

void Character::setInputObject(QObject *input)
{
  input->installEventFilter(this);
}

Entity &Character::getEntity()
{
  return *entity;
}

void Character::collectBonus(const BonusInfo &bonus)
{
  switch(bonus.effect)
    {
    case BonusInfo::Effect::IncreaseHp:
      music.acquireHP();
      break;

    case BonusInfo::Effect::IncreaseKarma:
      music.acquireKarma();
      break;

    case BonusInfo::Effect::DecreaseHp:
      music.lossHP();
      break;

    case BonusInfo::Effect::DecreaseKarma:
      music.lossKarma();
      break;

    case BonusInfo::Effect::None:
      return;
    }
}

void Character::releaseMouse()
{
  world.getInput(relPointerInputIndex).active = false;
}

void Character::tick(Entity &/*characterEntity*/, float sec_elapsed)
{
  QVector3D entityCenterPos = entity->getPosition();
  entityCenterPos.setY(entityCenterPos.y() + 0.5 * (entity->getBaseBoundBox().first.y() + entity->getBaseBoundBox().second.y()));

  if(lastEntityPos != entityCenterPos)
    {
      lastEntityPos = entityCenterPos;

      updateCamera();

      world.getIlluminationModel<KawaiiPbrLight>(0).changeDotLight(0, [this](KawaiiLamp &l) {
        QVector3D nPos = lastEntityPos + QVector3D(0,5,0);
        if(l.position != nPos)
          {
            l.position = nPos;
            return true;
          } else return false;
      });
    }

  QVector3D playersForce;
  if((input & InputCommand::Forward) == InputCommand::Forward)
    playersForce += QVector3D(0, 0,  0.75);
  if((input & InputCommand::Backward) == InputCommand::Backward)
    playersForce += QVector3D(0, 0, -0.75);
  if((input & InputCommand::Left) == InputCommand::Left)
    playersForce += QVector3D( 0.75, 0, 0);
  if((input & InputCommand::Right) == InputCommand::Right)
    playersForce += QVector3D(-0.75, 0, 0);

  const auto horisontalVel = QVector3D(entity->getVelocity().x(), 0, entity->getVelocity().z());
  const float animationSpeed = horisontalVel.lengthSquared() / (2.2 * 2.2);

  if(animationSpeed > 0 && animation)
    {
      animation->tick(animationTime += sec_elapsed * animationSpeed);
      entity->recomputeBaseBoundBox();
    }

  if(playersForce.isNull() || animationSpeed >= 1.0) return;

  playersForce *= 5.0 / playersForce.length();

  auto dir = cam.getQViewDir();
  dir.setY(0);

  if(dir.isNull()) return;

  const QQuaternion q(QQuaternion::rotationTo(QVector3D(0,0,1), dir));
  entity->push(sec_elapsed * q.rotatedVector(playersForce));

  if((input & InputCommand::Jump) == InputCommand::Jump)
    if(const float jumpPower = getJumpPower(); jumpPower == 1.0f)
      {
        doJump(jumpPower);
        jumpChargeStarted = std::chrono::steady_clock::now();
      }

  entity->setRotation(QQuaternion::rotationTo(QVector3D(0,0,1), QVector3D(-entity->getVelocity().x(), 0, entity->getVelocity().z())));
}

void Character::updateCamera()
{
  const QQuaternion q = QQuaternion::fromEulerAngles(eulerAngles);
  QMatrix4x4 trMat;

  const auto camPos = lastEntityPos + q.rotatedVector(QVector3D(0,0,-distance));

  trMat.lookAt(camPos,
               lastEntityPos,
               q.rotatedVector(QVector3D(0,1,0)));

  cam.setViewMat(trMat);

  world.getIlluminationModel<KawaiiPbrLight>(0).changeDotLight(1, [camPos](KawaiiLamp &l) {
    if(l.position != camPos)
      {
        l.position = camPos;
        return true;
      } else
      return false;
  });
}

void Character::rotateView(const QPoint &deltaMouse)
{
  eulerAngles.setX(eulerAngles.x() + 0.2 * deltaMouse.y());
  eulerAngles.setY(eulerAngles.y() - 0.2 * deltaMouse.x());
  updateCamera();
}

Character::InputCommand Character::getInputCommand(QKeyEvent *event)
{
  if(event->isAutoRepeat()) return InputCommand::None;

  switch(event->key())
    {
    case Qt::Key_W:
      return InputCommand::Forward;

    case Qt::Key_S:
      return InputCommand::Backward;

    case Qt::Key_A:
      return InputCommand::Left;

    case Qt::Key_D:
      return InputCommand::Right;

    case Qt::Key_Space:
      return InputCommand::Jump;

    default:
      {
        const QString txt = event->text().toLower();
        if(txt.contains(L'ц'))
          return InputCommand::Forward;
        else if(txt.contains(L'ы'))
            return InputCommand::Backward;
        else if(txt.contains(L'ф'))
            return InputCommand::Left;
        else if(txt.contains(L'в'))
            return InputCommand::Right;
      }
    }
  return InputCommand::None;
}

float Character::getJumpPower() const
{
  if(jumpChargeStarted == std::chrono::time_point<std::chrono::steady_clock>()) return 0;

  float jumpPower = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - jumpChargeStarted).count();
  jumpPower /= 500.0;
  jumpPower = std::clamp(jumpPower, 0.2f, 1.0f);
  return jumpPower;
}

void Character::doJump(float jumpPower)
{
  bool jumpAllowed = false;
  world.findCollision(entity, [this, &jumpAllowed] (const CollisionInfo &collision) -> bool {
      if(collision.a.body == entity)
        jumpAllowed += collision.normal.y() < 0;
      else
        jumpAllowed += collision.normal.y() > 0;
      return jumpAllowed;
    });

  if(jumpAllowed)
    entity->push(QVector3D(0, 5, 0) * jumpPower);
}

bool Character::eventFilter(QObject *, QEvent *event)
{
  auto key_event = static_cast<QKeyEvent*>(event);

  switch(event->type())
    {
    case QEvent::KeyPress:
      input |= getInputCommand(key_event);
      if((input & InputCommand::Jump) == InputCommand::Jump)
        {
          if(!key_event->isAutoRepeat())
            jumpChargeStarted = std::chrono::steady_clock::now();
        }
      break;

    case QEvent::KeyRelease:
      input &= ~getInputCommand(key_event);
      if(key_event->key() == Qt::Key_Space)
        doJump(getJumpPower());
      break;

    case QEvent::Wheel:
      {
        auto wheel = static_cast<QWheelEvent*>(event);
        QPoint numPixels = wheel->pixelDelta(),
            numDegrees = wheel->angleDelta();

        float delta = !numDegrees.isNull()? numDegrees.ry():
                                            !numPixels.isNull()? numPixels.ry() * 10.0:
                                                                 0;

        float hor_delta = !numDegrees.isNull()? numDegrees.rx():
                                                !numPixels.isNull()? numPixels.rx() * 10.0:
                                                                     0;

        distance -= delta / 500.0;
        world.res().setExposure(world.res().getExposure() - hor_delta / 500.0);
        updateCamera();
      }
      break;

    default: break;
    }

  return false;
}
