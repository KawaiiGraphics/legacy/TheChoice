#ifndef KILLER_HPP
#define KILLER_HPP

#include "Room.hpp"
#include "Entity.hpp"
#include <vector>

class Game;

class Killer
{
  Entity *entity;

  const std::vector<QVector3D> &path;
  std::vector<QVector3D>::const_iterator lastPos;
  const float speed;

  void goToNextPos(Entity &e);
  void onTick(Entity &e, float sec_elapsed);

public:
  Killer(Game &game, const Room::KillerInstance &info);
  ~Killer();

  Entity& getEntity();

  bool check(Entity *candidate);
};

#endif // KILLER_HPP
