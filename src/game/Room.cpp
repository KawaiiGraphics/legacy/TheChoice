#include "Room.hpp"

#include <QJsonValue>
#include <QJsonArray>
#include <QJsonObject>

namespace {
  Room::DoorInstance doorFromJson(const QJsonObject &json, Room &room)
  {
    const auto pos_v3 = Room::vec3FromJson(json.value("position").toArray());

    auto entity = room.getLocation().addEntityInfo("Door", "Door", ObjectType::Spirit);

    const Room::DoorInstance result = {
      .position = QVector2D(pos_v3.x(), pos_v3.z()),
      .entityInfo = entity,
      .rotation = static_cast<float>(json.value("rotation").toDouble(0.0))
    };
    entity->rotation = QQuaternion::fromEulerAngles(0, result.rotation, 0);
    entity->position = QVector3D(result.position.x(),
                                 /*doorHalfHeight*/ 2.5 - 0.5 * room.getSize().y(),
                                 result.position.y());

    return result;
  }

  Room::BonusInstance bonusFromJson(const QJsonObject &json, Room &room)
  {
    auto entity = room.getLocation().addEntityInfo("Textured", "modelName", ObjectType::Spirit);

    const Room::BonusInstance result = {
      .type = json.value("type").toString(),
      .position = Room::vec3FromJson(json.value("position").toArray()),
      .angleVel = Room::vec3FromJson(json.value("angle_vel").toArray()),
      .entityInfo = entity
    };

    entity->position = result.position;
    entity->angle_vel = result.angleVel;

    return result;
  }

  Room::KillerInstance killerFromJson(const QJsonObject &json, Room &room)
  {
    auto entity = room.getLocation().addEntityInfo("Textured", "modelName", ObjectType::Spirit);

    Room::KillerInstance result = {
      .type = json.value("type").toString(),
      .angleVel = Room::vec3FromJson(json.value("angle_vel").toArray()),
      .entityInfo = entity
    };
    entity->angle_vel = result.angleVel;

    for(const auto &j: json.value("path").toArray())
      result.path.push_back(Room::vec3FromJson(j.toArray()));
    if(!result.path.empty())
      entity->position = result.path.front();

    return result;
  }

  Room::Decoration decorationFromJson(const QJsonObject &json, ObjectType objType, Room &room)
  {
    Room::Decoration result = {
      .pos0 = Room::vec3FromJson(json.value("pos0").toArray()),
      .vel0 = Room::vec3FromJson(json.value("vel0").toArray()),
      .angleVel = Room::vec3FromJson(json.value("angleVel").toArray()),
      .model = json.value("model").toString(),
      .mass = static_cast<float>(objType == ObjectType::Wall? 1.0: json.value("mass").toDouble(1.0)),
    };
    auto entity = room.getLocation().addEntityInfo("Textured", result.model, objType);
    entity->position = result.pos0;
    entity->vel = result.vel0;
    entity->angle_vel = result.angleVel;
    entity->mass = result.mass;
    entity->frictionFactor = (0.15);

    result.entityInfo = entity;
    return result;
  }

  EntityInfo* cloneEntityInfo(const EntityInfo *orig, Location &location)
  {
    auto entity = location.addEntityInfo(orig->material, orig->model, orig->physicType);
    entity->vel = orig->vel;
    entity->mass = orig->mass;
    entity->onTick = orig->onTick;
    entity->position = orig->position;
    entity->rotation = orig->rotation;
    entity->angle_vel = orig->angle_vel;
    entity->onContact = orig->onContact;
    entity->lookForward = orig->lookForward;
    entity->restitution = orig->restitution;
    entity->frictionFactor = orig->frictionFactor;
    return entity;
  }

  Room::DoorInstance clone(const Room::DoorInstance &orig, Location &location)
  {
    Room::DoorInstance result;
    result.position = orig.position;
    result.rotation = orig.rotation;
    result.entityInfo = cloneEntityInfo(orig.entityInfo, location);
    return result;
  }

  Room::BonusInstance clone(const Room::BonusInstance &orig, Location &location)
  {
    Room::BonusInstance result;
    result.type = orig.type;
    result.angleVel = orig.angleVel;
    result.position = orig.position;
    result.entityInfo = cloneEntityInfo(orig.entityInfo, location);
    return result;
  }

  Room::KillerInstance clone(const Room::KillerInstance &orig, Location &location)
  {
    Room::KillerInstance result;
    result.path = orig.path;
    result.type = orig.type;
    result.angleVel = orig.angleVel;
    result.entityInfo = cloneEntityInfo(orig.entityInfo, location);
    return result;
  }

  Room::Decoration clone(const Room::Decoration &orig, Location &location)
  {
    Room::Decoration result;
    result.mass = orig.mass;
    result.pos0 = orig.pos0;
    result.vel0 = orig.vel0;
    result.model = orig.model;
    result.angleVel = orig.angleVel;
    result.entityInfo = cloneEntityInfo(orig.entityInfo, location);
    return result;
  }

  template<typename T>
  std::vector<T> clone(const std::vector<T> &orig, Location &location)
  {
    std::vector<T> result(orig.size());
    for(size_t i = 0; i < result.size(); ++i)
      result[i] = clone(orig[i], location);
    return result;
  }
}

Room::Room(const QJsonObject &jsonObject)
{
  size = vec3FromJson(jsonObject.value("size").toArray());

  enter = doorFromJson(jsonObject.value("enter").toObject(), *this);
  for(const auto &i: jsonObject.value("escape").toArray())
    escape.push_back(doorFromJson(i.toObject(), *this));

  for(const auto &i: jsonObject.value("bonuses").toArray())
    bonuses.push_back(bonusFromJson(i.toObject(), *this));

  for(const auto &i: jsonObject.value("killers").toArray())
    killers.push_back(killerFromJson(i.toObject(), *this));

  for(const auto &i: jsonObject.value("static_decorations").toArray())
    staticDecorations.push_back(decorationFromJson(i.toObject(), ObjectType::Wall, *this));

  for(const auto &i: jsonObject.value("dynamic_decorations").toArray())
    dynamicDecorations.push_back(decorationFromJson(i.toObject(), ObjectType::Solid, *this));
}

Room::Room(const Room &orig):
  size(orig.size)
{
  bonuses = clone(orig.bonuses, location);
  killers = clone(orig.killers, location);
  enter = clone(orig.enter, location);
  escape = clone(orig.escape, location);
  staticDecorations = clone(orig.staticDecorations, location);
  dynamicDecorations = clone(orig.dynamicDecorations, location);
}

const QVector3D &Room::getSize() const
{
  return size;
}

void Room::forallBonuses(const std::function<void (const BonusInstance&)> &func) const
{
  for(const auto &bonus: bonuses)
    func(bonus);
}

void Room::forallKillers(const std::function<void (const KillerInstance&)> &func) const
{
  for(const auto &killer: killers)
    func(killer);
}

void Room::forallStaticDecorations(const std::function<void (const Room::Decoration &)> &func) const
{
  for(const auto &i: staticDecorations)
    func(i);
}

void Room::forallDynamicDecorations(const std::function<void (const Room::Decoration &)> &func) const
{
  for(const auto &i: dynamicDecorations)
    func(i);
}

const Room::DoorInstance &Room::getEnter() const
{
  return enter;
}

void Room::forallEscapes(const std::function<void(const DoorInstance &)> &func) const
{
  for(const auto &esc: escape)
    func(esc);
}

QVector3D Room::vec3FromJson(const QJsonArray &arr)
{
  if(arr.size() < 3)
    return QVector3D();

  return QVector3D( arr.at(0).toDouble(), arr.at(1).toDouble(), arr.at(2).toDouble() );
}

void Room::finalizeBonuses(const QHash<QString, BonusInfo> &bonusessType)
{
  for(const auto &i: bonuses)
    {
      const auto &bonusInfo = bonusessType[i.type];
      i.entityInfo->model = bonusInfo.modelName;
    }
}

void Room::finalizeKillers(const QHash<QString, KillerInfo> &killersType)
{
  for(const auto &i: killers)
    {
      const auto &killerInfo = killersType[i.type];
      i.entityInfo->model = killerInfo.modelName;
    }
}

Location &Room::getLocation()
{
  return location;
}
