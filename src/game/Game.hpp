#ifndef GAME_HPP
#define GAME_HPP

#include <Kawaii3D/KawaiiRoot.hpp>
#include <thread>
#include <QDir>

#include "Character.hpp"
#include "World.hpp"
#include "Room.hpp"
#include "Killer.hpp"
#include "MusicMachine.hpp"

class Game
{
  struct BricksMaterial {
    QVector4D scale;

    QVector4D brickColor;
    QVector4D mortarColor;
    QVector2D brickSize;
    QVector2D brickPct;

    QVector2D brickProps;
    QVector2D mortarProps;

    float innerSide;
  };

  KawaiiRoot root;
  World world;

  QFuture<bool> playerModel;
  std::unique_ptr<Character> player;

  std::vector<Room> rooms;
  std::vector<size_t> story;

  QHash<QString, KillerInfo> killerTypes;
  QHash<QString, BonusInfo> bonusTypes;
  std::unordered_map<MusicMachine::State, std::unique_ptr<QImage>> endings;

  Entity *roomEntity;

  QDir configDir;

  std::vector<Entity*> inRoomEntities;
  std::vector<Killer*> inRoomKillers;

  MusicMachine music;

  MusicMachine::State ending;

  size_t roomIndex;

  size_t frameIndex;

  float doorHalfHeight;

  float currentFrametime;
  std::array<float, 1000> lastFrametimes;
public:
  Game();
  ~Game();

  void loadConfig(const QString &fName);

  World& getWorld();

  void goToRoom(size_t roomIndex);
  void goToNextRoom();
  void goToPrevRoom();

  Entity* bringKiller(const QString &type);
  float getKillerSpeed(const QString &type) const;

  void start();

  void finish();



  //IMPLEMeNT
private:
  void loadModels(const QJsonArray &json);
  void loadMusic(const QJsonObject &json);
  void loadEndings(const QJsonObject &json);
  void loadKillerTypes(const QJsonArray &json);
  void loadBonusTypes(const QJsonArray &json);
  void loadRooms(const QJsonArray &json);
  void loadStory(const QJsonArray &json);

  void drawOverlay(QPainter &painter);

  void clearRoom();

  const Room& currentRoom() const;
  Entity *bringDoor(const Room::DoorInstance &door);
  Entity *bringBonus(const Room::BonusInstance &bonus);
  Entity *bringDecoration(const Room::Decoration &decoration, ObjectType objType);
};

#endif // GAME_HPP
