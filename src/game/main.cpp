#include <QGuiApplication>
#include "Game.hpp"

int main(int argc, char *argv[])
{
  QGuiApplication app(argc, argv);
  app.setApplicationName(APP_NAME);

  Game game;
  game.loadConfig(argv[1]);
  game.start();

  return app.exec();
}
