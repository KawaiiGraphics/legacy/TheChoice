@echo off
SET MAKEFLAGS=-j%NUMBER_OF_PROCESSORS%
SET _script_dir=%~dp0
SET _distr_dir=%_script_dir%distr
SET _build_dir=%_script_dir%build
SET _cmake_prefix_path=%_distr_dir%\lib\cmake;C:\Qt\5.12.6\mingw73_64\lib\cmake

mkdir %_distr_dir%
mkdir %_build_dir%

CALL:build sib_utils
CALL:build glm
CALL:build Kawaii3D
CALL:build KawaiiRenderer
CALL:build KurisuRenderer
CALL:build MisakaRenderer
CALL:build KawaiiFigures3D
::CALL:build assimp
::CALL:build KawaiiAssimp
CALL:build KawaiiWorlds
CALL:build TheChoice

IF EXIST %_distr_dir%\bin\NUL (
	COPY "%_distr_dir%\lib\*.dll" %_distr_dir%\bin
)

EXIT /B %ERRORLEVEL%

:build
	mkdir %_build_dir%\%~1
	pushd %_build_dir%\%~1


	cmake -G "MinGW Makefiles" -DCMAKE_CXX_COMPILER=g++ -DCMAKE_BUILD_TYPE=Release -DENABLE_TESTS=OFF -DCMAKE_INSTALL_PREFIX=%_distr_dir% -DCMAKE_PREFIX_PATH=%_cmake_prefix_path% %_script_dir%%~1%
	cmake --build .
	cmake --build . --target install

	IF EXIST %_script_dir%\%~1%\Common\NUL (
		mkdir common
		pushd common
		cmake -G "MinGW Makefiles" -DCMAKE_CXX_COMPILER=g++ -DCMAKE_INSTALL_PREFIX=%_distr_dir% "%_script_dir%\%~1%\Common"
		cmake --build .
		cmake --build . --target install
		popd '../'
	)

	popd
EXIT /B 0

